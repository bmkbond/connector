package com.tarang.pgconnector;

import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SendISORequestPacket {

	@Test
	public void sendISOPacket() throws Exception {
		try{
		GenericPackager packager = new GenericPackager("basic.xml");
		String data = "60000000000100822000000000000000000000000001000929155751000062303239400A30303133313130343638450F363239393030333020202020202020";
		System.out.println("DATA : " + data);
		ISOMsg isoMsg = new ISOMsg();
		isoMsg.setPackager(packager);
		isoMsg.unpack(data.getBytes());
		}catch(Exception e){
			System.out.println(e);
		}
	}

}
