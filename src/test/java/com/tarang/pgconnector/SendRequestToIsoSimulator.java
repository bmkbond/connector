package com.tarang.pgconnector;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tarang.pgconnector.service.HostService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SendRequestToIsoSimulator {

	@Autowired
	private HostService hostService;

	@Test
	public void sendRequestPacketToISOSimulator() {
		GenericPackager packager;
		try {
			packager = new GenericPackager("basic.xml");

			ISOMsg isoMsg = new ISOMsg();
			isoMsg.setPackager(packager);
			isoMsg.setMTI("0100");//mti
			isoMsg.set(2, "4111111111111111");//cardno
			isoMsg.set(3, "000000");//processingcode
			isoMsg.set(4, new BigDecimal("1").movePointRight(2).toString());//amount with cents
			Date date = new Date();
			DateFormat format = new SimpleDateFormat("yyyyMMddHH");
			TimeZone sgtTime = TimeZone.getTimeZone("Asia/Singapore");
			format.setTimeZone(sgtTime);
			String localDate = format.format(date);
			isoMsg.set(7, localDate);//localtime
			Random rand = new Random();
			int orderNo = rand.nextInt(1000000);
			isoMsg.set(11, String.valueOf(orderNo));//order number
			isoMsg.set(14, "1120");//ExpriyDate
			// if(paymentRequest.getEntryMode().equals("ecommerce")) 
			isoMsg.set(22, "100");//type of transaction
			isoMsg.set(37, "123456");//rrn
			isoMsg.set(40,"123");//cvv
			isoMsg.set(42, "12121212");//card acceptor identification code
			isoMsg.set(44, "A5DFGR");// additional response data
			isoMsg.set(49, "INR");//currency

			ISOMsg isoResponse = hostService.send(isoMsg);
			System.out.println("Response Recieved  " + isoResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
