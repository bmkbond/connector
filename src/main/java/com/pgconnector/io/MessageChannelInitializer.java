package com.pgconnector.io;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;

import javax.net.ssl.KeyManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;





import com.tarang.pgconnector.io.MessageClientHandler;
import com.tarang.pgconnector.io.MessageDecoder;
import com.tarang.pgconnector.io.MessageEncoder;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;

@Service
public class MessageChannelInitializer extends ChannelInitializer<SocketChannel> {

    private static final Logger log = LoggerFactory.getLogger(MessageChannelInitializer.class);
    @Autowired
    private MessageEncoder dciEncoder;
    @Autowired
    private MessageDecoder dciDecoder;
    @Autowired
    private MessageClientHandler messageClientHandler;

    @Value("${ssl.enabled}")
    private String sslEnabled;

    @Value("${ssl.keystorePath}")
    private String sslkeystorePath;

    @Value("${ssl.keystorePassword}")
    private String sslkeystorePassword;

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        log.debug("Initialize Channel {}", channel);
        ChannelPipeline pipeline = channel.pipeline();
        if (Boolean.parseBoolean("false")) {
            log.debug("ssl Enabled");
            SslContextBuilder ctxBuilder = SslContextBuilder.forClient().keyManager(getKeyManager());
            
            SslContext sslCtx = ctxBuilder.build();
            pipeline.addLast("ssl", sslCtx.newHandler(channel.alloc()));
        }
        else {
            log.debug("ssl not Enabled");
        }
        pipeline.addLast(dciEncoder);
        pipeline.addLast(dciDecoder);
        pipeline.addLast("handler", messageClientHandler);
    }

    private KeyManagerFactory getKeyManager() throws Exception {

        KeyStore ks = null;
        FileInputStream fis = null;
        try {
            ks = KeyStore.getInstance(KeyStore.getDefaultType());
            fis = new java.io.FileInputStream(sslkeystorePath);
            ks.load(fis, sslkeystorePassword.toCharArray());
        } catch (KeyStoreException e) {
            log.error("KeyStoreException caught in getKeyManager() method in  DciChannelInitializer class",
                    e.toString());
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
        KeyManagerFactory kmf;
        kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(ks, sslkeystorePassword.toCharArray());
        return kmf;
    }

}