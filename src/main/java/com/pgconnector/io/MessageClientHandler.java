package com.pgconnector.io;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Sharable
@Service
public class MessageClientHandler extends SimpleChannelInboundHandler<ISOMsg> {

    private static final Logger log = LoggerFactory.getLogger(MessageClientHandler.class);
    private ISOMsg message;
    final BlockingQueue<ISOMsg> answer = new LinkedBlockingQueue<ISOMsg>();

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ISOMsg msg) throws Exception {
        answer.offer(msg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        log.error("Exception Caught {} ", cause.toString());
        ctx.close();
    }

    public ISOMsg getMessage() {
        try {
            message = answer.take();
        } catch (InterruptedException e) {
            log.error("Exception caught in getMessage method  DciClientHandler class", e.toString());
        }
        return message;
    }

}