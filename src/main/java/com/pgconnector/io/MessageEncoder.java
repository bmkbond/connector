package com.pgconnector.io;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Hex;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

@Sharable
@Service
public class MessageEncoder extends MessageToByteEncoder<ISOMsg> {

    private static final Logger log = LoggerFactory.getLogger(MessageEncoder.class);

    private static final int DEFAULT_BUFFER_LENGTH = 256;
    
    private Map<Integer, Object> fields = new HashMap<Integer, Object>();
    String ETX = "03";
  

    @Override
    public void encode(ChannelHandlerContext ctx, ISOMsg msg, ByteBuf out) throws Exception {

        final ByteArrayOutputStream os = new ByteArrayOutputStream(DEFAULT_BUFFER_LENGTH);
        try {
        	System.out.println("encode......");
            logISOMsg(msg);
            byte[] data = msg.pack();
            /*String hexReq = Hex.encodeHexString(data);//toHexString(data);
            int len = hexReq.length();
            byte[] dataq = new byte[len / 2];
            for (int i = 0; i < len; i += 2) {
                data[i / 2] = (byte) ((Character.digit(hexReq.charAt(i), 16) << 4)
                                     + Character.digit(hexReq.charAt(i+1), 16));
            }
           // int length = os.size();
            StringBuilder sb = new StringBuilder();
            sb.append(toHexString(getStrNumber(4, dataq.length)));
            sb.append(toHexString(hexReq));
            sb.append(ETX);
            byte[] req = getByteArray(sb.toString());*/
            log.info("Sending Request  to Host : {} ", Hex.encodeHexString(data));
            out.writeBytes(data);

        } catch (Exception e) {
            log.error("Unable to encode data: " + e);
            throw new IllegalStateException("unable to encode data, " + e.getMessage());
        }
    }

    public static String getHexString(byte[] b) throws Exception {
        String result = "";
        for (int i = 0; i < b.length; i++) {
            result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }
    
       
    public static String getStrNumber(int length, int num) {
        String s = "" + num;
        if (s.length() > length) {
            throw new RuntimeException("Number length is more than the expected..");
        }
        for (int i = s.length(); i < length; i++) {
            s = "0" + s;
        }
        return s;
    }
    
    public static String toHexString(String str) {
        byte[] b = str.getBytes();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            sb.append(Integer.toHexString((b[i] & 0xF0) >> 4).toUpperCase());
            sb.append(Integer.toHexString(b[i] & 0x0F).toUpperCase());
        }
        return sb.toString();
    }
    public static byte[] getByteArray(String hexString) {
        if (hexString == null) {
            return null;
        }

        int len = hexString.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4)
                    + Character.digit(hexString.charAt(i + 1), 16));
        }
        return data;
    }
    
    public static String toHexString(byte[] array) {
        return DatatypeConverter.printHexBinary(array);
    }

    private static void logISOMsg(ISOMsg msg) {
		System.out.println("----ISO MESSAGE-----");
		try {
			System.out.println("  MTI : " + msg.getMTI());
			for (int i=1;i<=msg.getMaxField();i++) {
				if (msg.hasField(i)) {
					System.out.println("    Field-"+i+" : "+msg.getString(i));
				}
			}
		} catch (ISOException e) {
			e.printStackTrace();
		} finally {
			System.out.println("--------------------");
		}
 
	}
    
    
}