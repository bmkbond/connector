package com.pgconnector.io;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.bytes.ByteArrayDecoder;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Service;

@Configuration
@ImportResource({"classpath*:basic.xml"})

@Sharable
@Service
public class MessageDecoder extends ByteArrayDecoder {

    private static final Logger log = LoggerFactory.getLogger(MessageDecoder.class);

    @Override
    public void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

        try {
        	System.out.println("decode....");
        	GenericPackager packager = new GenericPackager("basic.xml");
            if (in.isReadable()) {
               /* byte[] isoMessageLength = new byte[in.readableBytes()];
                in.getBytes(0, isoMessageLength);
                byte[] header = new byte[2];
                byte[] message = new byte[in.readableBytes() - 1];
                in.getBytes(0, header);
                in.getBytes(0, message);
                log.debug("Response from host: {} ", toHexString(message));
                in.getBytes(4, message);
                ISOMsg isoMsg = new ISOMsg();
                isoMsg.setPackager(packager);
                isoMsg.unpack(message);
                in.clear();
                logISOMsg(isoMsg);
                out.add(isoMsg);*/
            	
            	
                 byte[] message = new byte[in.readableBytes()];
                 in.getBytes(0, message);
                 log.debug("Response from host: {} ", toHexString(message));
                 System.out.println("Response from host: {} "+ toHexString(message));
                 ISOMsg isoMsg = new ISOMsg();
                 isoMsg.setPackager(packager);
                 isoMsg.unpack(message);
                 in.clear();
                 logISOMsg(isoMsg);
                 out.add(isoMsg);
            }

        } catch (Exception e) {
            log.error("Unexpected exception while decoding {}", e);
            throw new IllegalArgumentException("unexpected exception", e);
        }

    }
    
    public static String toHexString(String str) {
        byte[] b = str.getBytes();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            sb.append(Integer.toHexString((b[i] & 0xF0) >> 4).toUpperCase());
            sb.append(Integer.toHexString(b[i] & 0x0F).toUpperCase());
        }
        return sb.toString();
    }
    
    public static void logISOMsg(ISOMsg msg) {
		System.out.println("----ISO MESSAGE-----");
		try {
			System.out.println("  MTI : " + msg.getMTI());
			for (int i=1;i<=msg.getMaxField();i++) {
				if (msg.hasField(i)) {
					System.out.println("    Field-"+i+" : "+msg.getString(i));
				}
			}
		} catch (ISOException e) {
			e.printStackTrace();
		} finally {
			System.out.println("--------------------");
		}
 
	}
    public static String toHexString(byte[] array) {
        return DatatypeConverter.printHexBinary(array);
    }

}
