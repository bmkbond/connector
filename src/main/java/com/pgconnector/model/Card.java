package com.pgconnector.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Card {


    
    @JacksonXmlProperty(localName = "account-number")
    private String accountNumber;

    @JacksonXmlProperty(localName = "expiration-month")
    private String expirationMonth;
    
    @JacksonXmlProperty(localName = "expiration-year")
    private String expirationYear;
    
    @JacksonXmlProperty(localName = "card-type")
    private String cardType;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getExpirationMonth() {
		return expirationMonth;
	}

	public void setExpirationMonth(String expirationMonth) {
		this.expirationMonth = expirationMonth;
	}

	public String getExpirationYear() {
		return expirationYear;
	}

	public void setExpirationYear(String expirationYear) {
		this.expirationYear = expirationYear;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	


	
    
}
