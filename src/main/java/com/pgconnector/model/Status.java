package com.pgconnector.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Status {

    
    public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JacksonXmlProperty(localName = "code" , isAttribute=true)
    private String code;

    @JacksonXmlProperty(localName = "description" , isAttribute=true)
    private String description;
    
    @JacksonXmlProperty(localName = "severity" , isAttribute=true)
    private String severity;
    
    @JacksonXmlProperty(localName = "provider-transaction-id" , isAttribute=true)
    private String providerTransactionId;

	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getProviderTransactionId() {
		return providerTransactionId;
	}

	public void setProviderTransactionId(String providerTransactionId) {
		this.providerTransactionId = providerTransactionId;
	}
	
    
}
