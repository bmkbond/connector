package com.pgconnector.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "payment")
public class Payment implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JacksonXmlProperty(localName = "payment-methods")
	private List<PaymentMethod> paymentMethods;

	@JacksonXmlProperty(localName = "merchant-account-id")
	private String merchantAccountId;

	@JacksonXmlProperty(localName = "transaction-type")
	private String transactionType;

	@JacksonXmlProperty(localName = "request-id")
	private String requestId;

	@JacksonXmlProperty(localName = "requested-amount")
	private RequestedAmount requestedAmount;

	@JacksonXmlProperty(localName = "account-holder")
	private AccountHolder accountHolder;

	@JacksonXmlProperty(localName = "shipping")
	private Shipping shipping;

	@JacksonXmlProperty(localName = "periodic")
	private Periodic periodic;

	@JacksonXmlProperty(localName = "card")
	private Card card;

	@JacksonXmlProperty(localName = "order-number")
	private String orderNumber;

	@JacksonXmlProperty(localName = "descriptor")
	private String descriptor;

	@JacksonXmlProperty(localName = "entry-mode")
	private String entryMode;

	

	public List<PaymentMethod> getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public String getMerchantAccountId() {
		return merchantAccountId;
	}

	public void setMerchantAccountId(String merchantAccountId) {
		this.merchantAccountId = merchantAccountId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public RequestedAmount getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(RequestedAmount requestedAmount) {
		this.requestedAmount = requestedAmount;
	}

	public AccountHolder getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(AccountHolder accountHolder) {
		this.accountHolder = accountHolder;
	}

	public Shipping getShipping() {
		return shipping;
	}

	public void setShipping(Shipping shipping) {
		this.shipping = shipping;
	}

	public Periodic getPeriodic() {
		return periodic;
	}

	public void setPeriodic(Periodic periodic) {
		this.periodic = periodic;
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getDescriptor() {
		return descriptor;
	}

	public void setDescriptor(String descriptor) {
		this.descriptor = descriptor;
	}

	public String getEntryMode() {
		return entryMode;
	}

	public void setEntryMode(String entryMode) {
		this.entryMode = entryMode;
	}

}
