package com.pgconnector.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "payment")
public class PaymentResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JacksonXmlProperty(localName = "merchant-account-id")
	private String merchantAccountId;

	@JacksonXmlProperty(localName = "transaction-id")
	private String transactionId;

	@JacksonXmlProperty(localName = "request-id")
	private String requestId;

	@JacksonXmlProperty(localName = "transaction-type")
	private String transactionType;
	
	@JacksonXmlProperty(localName = "transaction-state")
	private String transactionState;
	
	@JacksonXmlProperty(localName = "completion-time-stamp")
	private String completionTimeStamp;
	
	
	@JacksonXmlProperty(localName = "statuses")
	private Statuses statuses;

	@JacksonXmlProperty(localName = "csc-code")
	private String cscCode;
	
	@JacksonXmlProperty(localName = "requested-amount")
	private RequestedAmount requestedAmount;
	
	@JacksonXmlProperty(localName = "account-holder")
	private AccountHolder accountHolder;

	@JacksonXmlProperty(localName = "shipping")
	private Shipping shipping;

	@JacksonXmlProperty(localName = "card-token")
	private CardToken cardToken;

	@JacksonXmlProperty(localName = "ip-address")
	private String ipAddress;

	@JacksonXmlProperty(localName = "order-number")
	private String orderNumber;

	@JacksonXmlProperty(localName = "descriptor")
	private String descriptor;
	
	@JacksonXmlProperty(localName = "payment-methods")
	private PaymentMethods paymentMethods;
	
	
	@JacksonXmlProperty(localName = "authorization-code")
	private String authorizationCode;
	
	@JacksonXmlProperty(localName = "three-d")
	private Threed threed;
	
	@JacksonXmlProperty(localName = "api-id")
	private String apiId;
	
	
	@JacksonXmlProperty(localName = "entry-mode")
	private String entryMode;
	
	

	

	

	public PaymentMethods getPaymentMethods() {
		return paymentMethods;
	}

	public void setPaymentMethods(PaymentMethods paymentMethods) {
		this.paymentMethods = paymentMethods;
	}

	public String getMerchantAccountId() {
		return merchantAccountId;
	}

	public void setMerchantAccountId(String merchantAccountId) {
		this.merchantAccountId = merchantAccountId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public RequestedAmount getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(RequestedAmount requestedAmount) {
		this.requestedAmount = requestedAmount;
	}

	public AccountHolder getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(AccountHolder accountHolder) {
		this.accountHolder = accountHolder;
	}

	public Shipping getShipping() {
		return shipping;
	}

	public void setShipping(Shipping shipping) {
		this.shipping = shipping;
	}

	

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionState() {
		return transactionState;
	}

	public void setTransactionState(String transactionState) {
		this.transactionState = transactionState;
	}

	public String getCompletionTimeStamp() {
		return completionTimeStamp;
	}

	public void setCompletionTimeStamp(String completionTimeStamp) {
		this.completionTimeStamp = completionTimeStamp;
	}

	public Statuses getStatuses() {
		return statuses;
	}

	public void setStatuses(Statuses statuses) {
		this.statuses = statuses;
	}

	public String getCscCode() {
		return cscCode;
	}

	public void setCscCode(String cscCode) {
		this.cscCode = cscCode;
	}

	public CardToken getCardToken() {
		return cardToken;
	}

	public void setCardToken(CardToken cardToken) {
		this.cardToken = cardToken;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public Threed getThreed() {
		return threed;
	}

	public void setThreed(Threed threed) {
		this.threed = threed;
	}

	public String getApiId() {
		return apiId;
	}

	public void setApiId(String apiId) {
		this.apiId = apiId;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getDescriptor() {
		return descriptor;
	}

	public void setDescriptor(String descriptor) {
		this.descriptor = descriptor;
	}

	public String getEntryMode() {
		return entryMode;
	}

	public void setEntryMode(String entryMode) {
		this.entryMode = entryMode;
	}

}
