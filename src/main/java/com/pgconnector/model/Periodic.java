package com.pgconnector.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Periodic {


    
    @JacksonXmlProperty(localName = "periodic-type")
    private String periodicType;

	public String getPeriodicType() {
		return periodicType;
	}

	public void setPeriodicType(String periodicType) {
		this.periodicType = periodicType;
	}


	
    
}
