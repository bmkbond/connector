package com.pgconnector.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class AccountHolder {


	
	@JacksonXmlProperty(localName = "first-name")
    private String firstName;
	
    @JacksonXmlProperty(localName = "last-name")
    private String lastName;
    
    @JacksonXmlProperty(localName = "email")
    private String email;
    
    @JacksonXmlProperty(localName = "phone")
    private String phone;
    
    
    @JacksonXmlProperty(localName = "address")
    private Address address;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

    
}
