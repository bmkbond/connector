package com.pgconnector.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Threed {

/*	@JacksonXmlProperty(localName = "three-d")
	private String threed;
*/
	@JacksonXmlProperty(localName = "cardholder-authentication-status")
	private String cardholderAuthenticationStatus;

	/*public String getThreed() {
		return threed;
	}

	public void setThreed(String threed) {
		this.threed = threed;
	}*/

	public String getCardholderAuthenticationStatus() {
		return cardholderAuthenticationStatus;
	}

	public void setCardholderAuthenticationStatus(String cardholderAuthenticationStatus) {
		this.cardholderAuthenticationStatus = cardholderAuthenticationStatus;
	}

}
