package com.pgconnector.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CardToken {

    @JacksonXmlProperty(localName = "token-id")
    private String tokenId;
    
    @JacksonXmlProperty(localName = "masked-account-number")
	private String maskedAccountNumber;

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

	public String getMaskedAccountNumber() {
		return maskedAccountNumber;
	}

	public void setMaskedAccountNumber(String maskedAccountNumber) {
		this.maskedAccountNumber = maskedAccountNumber;
	}
    
}
