package com.pgconnector.model;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Statuses {


	@JacksonXmlProperty(localName = "status"   )
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<Status> status;

	public List<Status> getStatus() {
		return status;
	}

	public void setStatus(List<Status> status) {
		this.status = status;
	}

	

	

}
