package com.pgconnector.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Shipping {


    
    @JacksonXmlProperty(localName = "address")
    private Address address;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

    
}
