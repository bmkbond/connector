package com.pgconnector.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

public class RequestedAmount {


    @JacksonXmlProperty(localName = "currency" ,isAttribute = true)
    private String currency;
    
    @JacksonXmlText(value = true)
    private String requestedAmount;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getRequestedAmount() {
		return requestedAmount;
	}

	public void setRequestedAmount(String requestedAmount) {
		this.requestedAmount = requestedAmount;
	}
	

    
    
}
