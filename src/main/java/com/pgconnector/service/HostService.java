package com.pgconnector.service;

import org.jpos.iso.ISOMsg;

public interface HostService {

	public ISOMsg send(ISOMsg request) throws Exception;
}