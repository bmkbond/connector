package com.pgconnector.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Service;

import com.tarang.pgconnector.model.AccountHolder;
import com.tarang.pgconnector.model.Address;
import com.tarang.pgconnector.model.CardToken;
import com.tarang.pgconnector.model.Payment;
import com.tarang.pgconnector.model.PaymentMethod;
import com.tarang.pgconnector.model.PaymentMethods;
import com.tarang.pgconnector.model.PaymentResponse;
import com.tarang.pgconnector.model.RequestedAmount;
import com.tarang.pgconnector.model.Shipping;
import com.tarang.pgconnector.model.Status;
import com.tarang.pgconnector.model.Statuses;
import com.tarang.pgconnector.model.Threed;
import com.tarang.pgconnector.service.ConnectorService;
import com.tarang.pgconnector.service.HostService;

/**
 * This class is used for implementing the all Payment Gateway operations
 * 
 * @author radhakrishnab
 *
 */
@Configuration
@ImportResource({"classpath*:basic.xml"})

@Service("connectorService")
public class ConnectorServiceImpl implements ConnectorService {

	private static final Logger log = LoggerFactory.getLogger(ConnectorServiceImpl.class);
	
	@Autowired
	private HostService hostService;
	
	

	/**
	 * This method used to make payment
	 * 
	 * @param PGTransctionRequest
	 *            pgTransctionRequest
	 * @return PGTransactionResponse pgTransactionResponse
	 * @throws Exception
	 */
	@Override
	public PaymentResponse processTransaction(Payment paymentRequest) throws Exception {
		log.info("processTransaction intitiated");
		PaymentResponse paymentResponse = new PaymentResponse();
		
		switch (paymentRequest.getTransactionType()) {

		case "authorization":
			GenericPackager packager = new GenericPackager("basic.xml");
			ISOMsg isoMsg = new ISOMsg();
			isoMsg.setPackager(packager);
			isoMsg.setMTI("0100");
			isoMsg.set(2, paymentRequest.getCard().getAccountNumber());
			isoMsg.set(3, "000000");
			BigDecimal val=new BigDecimal(paymentRequest.getRequestedAmount().getRequestedAmount()).movePointRight(2);
			isoMsg.set(4,val.toString() );
			Date date=new Date();
			DateFormat format = new SimpleDateFormat("yyyyMMddHH");
	        TimeZone sgtTime = TimeZone.getTimeZone("Asia/Singapore");
	        format.setTimeZone(sgtTime);
	        String localDate=format.format(date);
			isoMsg.set(7, localDate );
			Random rand = new Random();
			int orderNo=rand.nextInt(1000000);
			isoMsg.set(11, String.valueOf(orderNo));
			String cardExpiry=paymentRequest.getCard().getExpirationYear().substring(1, 3)+paymentRequest.getCard().getExpirationMonth();
			isoMsg.set(14, cardExpiry);
			if(paymentRequest.getEntryMode().equals("ecommerce")) {
				isoMsg.set(22, "100");
			}
			
			isoMsg.set(37, paymentRequest.getOrderNumber());
			//isoMsg.set(40,pgTransctionRequest.getCardCVV()); 
			isoMsg.set(42,paymentRequest.getMerchantAccountId()); 
			isoMsg.set(44, "A5DFGR");
			isoMsg.set(49, paymentRequest.getRequestedAmount().getCurrency());		
			
			ISOMsg isoResponse = hostService.send(isoMsg);
			System.out.println("Response Recieved  "+isoResponse);
			paymentResponse.setMerchantAccountId(isoResponse.getString(42));
			paymentResponse.setTransactionId(isoResponse.getString(42));
			paymentResponse.setRequestId(isoResponse.getString(37));
			paymentResponse.setTransactionType(paymentRequest.getTransactionType());
			SimpleDateFormat format1 = new SimpleDateFormat(
				    "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
				format.setTimeZone(TimeZone.getTimeZone("UTC"));
	        TimeZone sgtTime1 = TimeZone.getTimeZone("Asia/Singapore");
	        format1.setTimeZone(sgtTime);
	        String localDate1=format1.format(date);
			paymentResponse.setCompletionTimeStamp(localDate1);
			Statuses statuses=new Statuses();
			List<Status> statusCodesList=new ArrayList<Status>();
			Status status = new Status();
			status.setCode("201.0000");
			status.setDescription("3d-acquirer:The resource was successfully created.");
			status.setSeverity("information");
			status.setProviderTransactionId("C210079146944293986038");
			statusCodesList.add(status);
			Status status1 = new Status();
			status1.setCode("200.1083");
			status1.setDescription("3d-acquirer:Cardholder Successfully authenticated.");
			status1.setSeverity("information");
			status1.setProviderTransactionId("C210079146944293986038");
			statusCodesList.add(status1);
			statuses.setStatus(statusCodesList);
			paymentResponse.setStatuses(statuses);
			paymentResponse.setCscCode("M");
			RequestedAmount requestedAmount=new RequestedAmount();
			BigDecimal amount = new BigDecimal((String) isoResponse.getValue(4)).movePointLeft(2);
			requestedAmount.setRequestedAmount(amount.toString());
			requestedAmount.setCurrency((String) isoResponse.getValue(49));
			paymentResponse.setRequestedAmount(requestedAmount);
			AccountHolder accountHolder=new AccountHolder();
			accountHolder.setFirstName(paymentRequest.getAccountHolder().getFirstName());
			accountHolder.setFirstName(paymentRequest.getAccountHolder().getLastName());
			accountHolder.setEmail("brikluckman@yahoo.com");
			accountHolder.setPhone("2402415793");
			Address address=new Address();
			address.setCountry("XXXX");
			accountHolder.setAddress(address);
			Shipping shipping=new Shipping();
			shipping.setAddress(address);
			paymentResponse.setAccountHolder(accountHolder);
			CardToken cardToken=new CardToken();
			cardToken.setTokenId("4634397202557405");
			cardToken.setMaskedAccountNumber("411111******1111");
			paymentResponse.setCardToken(cardToken);
			paymentResponse.setIpAddress("120.29.149.56");
			paymentResponse.setOrderNumber(paymentRequest.getOrderNumber());
			paymentResponse.setDescriptor("SRS Travel");
			//PaymentMethods paymentMethods=new PaymentMethods();
			List<PaymentMethod> paymentMethodList=new ArrayList<PaymentMethod>();
			PaymentMethod paymentMethod=new PaymentMethod();
			paymentMethod.setName("creditcard");
			paymentMethodList.add(paymentMethod);
			PaymentMethods paymentMethods=new PaymentMethods();
			paymentMethods.setPaymentMethod(paymentMethodList);
			paymentResponse.setPaymentMethods(paymentMethods);
			paymentResponse.setAuthorizationCode((String)isoResponse.getValue(38));
			Threed threed=new Threed();
			threed.setCardholderAuthenticationStatus("Y");
			paymentResponse.setThreed(threed);
			if(isoResponse.getValue(39).equals("00")) {
				paymentResponse.setTransactionState("success");
			}else {
				paymentResponse.setTransactionState("Fail");
			}
			paymentResponse.setApiId("elastic-api");
			paymentResponse.setEntryMode("ecommerce");
			
			break;

		default:
			throw new IllegalArgumentException("Transaction could not be proccessed ");
		}

		return paymentResponse;
	}

}