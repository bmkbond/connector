package com.pgconnector.service.impl;

import org.jpos.iso.ISOMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.tarang.pgconnector.io.MessageChannelInitializer;
import com.tarang.pgconnector.io.MessageClientHandler;
import com.tarang.pgconnector.service.HostService;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;




@Service("hostService")
public class HostServiceImpl implements HostService {

    private static final Logger log = LoggerFactory.getLogger(HostServiceImpl.class);

    @Autowired
    private MessageChannelInitializer messageChannelInitializer;

    private Bootstrap bootstrap;

    private EventLoopGroup group;

    
    @Value("${host.address}")
    private String dciAddress;
    
    @Value("${host.port}")
    private String hostPort;
    

    @Override
    public ISOMsg send(ISOMsg isoRequest) throws Exception {

        try {
            bootstrap = new Bootstrap();
            group = new NioEventLoopGroup();
            bootstrap.group(group).channel(NioSocketChannel.class).handler(messageChannelInitializer);
            ChannelFuture channelFuture = bootstrap
                    .connect(dciAddress, Integer.valueOf(hostPort)).sync();
            Channel channel = channelFuture.channel();
            channel.writeAndFlush(isoRequest);
            log.info("request sent to host: {}", channel.isWritable());
            if (channel.isWritable()) {
                log.info("Host address: {}", channel.remoteAddress());
                Channel channelRead = channel.read();
                MessageClientHandler handler = (MessageClientHandler) channelRead.pipeline().last();
                ISOMsg message = handler.getMessage();
                log.info("Response from host: {}", message);
                return message;
            } else {
                throw new Exception("Operation timeout: Can't write.");
            }
        }  catch (Exception e) {
            log.error("Unknown network error {}", e);
            throw new Exception("unknown network error", e);
        } finally {
            group.shutdownGracefully();
        }
    }

   

}