package com.pgconnector.service;

import com.pgconnector.model.Payment;
import com.pgconnector.model.PaymentResponse;

/**
 * This interface defines the all gateway operations
 * 
 * @author radhakrishnab
 *
 */

public interface ConnectorService {

	public PaymentResponse processTransaction(Payment paymentRequest) throws Exception;

}
