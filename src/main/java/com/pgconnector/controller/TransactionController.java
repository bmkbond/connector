package com.pgconnector.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.tarang.pgconnector.model.PGTransactionResponse;
import com.tarang.pgconnector.model.PGTransctionRequest;
import com.tarang.pgconnector.model.Payment;
import com.tarang.pgconnector.model.PaymentResponse;
import com.tarang.pgconnector.service.ConnectorService;



@RestController
@RequestMapping("/pg-connector")
public class TransactionController {
	

@Autowired
private ConnectorService connectorService;

	@PostMapping(path = "/process-txn",consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> processPayment(@RequestBody String transctionRequest)
			throws Exception {
		String transactionResponse="";
		/*String transactionResponse="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><payment xmlns=\"http://www.elastic-payments.com/schema/payment\" \r\n" + 
				"xmlns:ns2=\"http://www.elastic-payments.com/schema/epa/transaction\" \r\n" + 
				"self=\"http://api.wirecard.com/engine/rest/merchants/1f4dd9d2-ddd7-4216-892f-1b8ae7329343/payments/ce0d43c5-9206-42c8-abb1-d45f1cfe6bca\"><merchant-account-id\r\n" + 
				"ref=\"http://api.wirecard.com/engine/rest/merchants/1f4dd9d2-ddd7-4216-892f-1b8ae7329343\">1f4dd9d2-ddd7-4216-892f-1b8ae7329343</merchant-account-id><transaction-id>ce0d43c5-9206-42c8-abb1-d45f1cfe6bca</transaction-id><request-id>16874025072016103225</request-id><transaction-type>authorization</transaction-type><transaction-state>success</transaction-state><completion-time-stamp>2016-07-25T10:35:40.000Z</completion-time-stamp><statuses><status\r\n" + 
				"code=\"201.0000\" description=\"3d-acquirer:The resource was successfully created.\" severity=\"information\" \r\n" + 
				"provider-transaction-id=\"C210079146944293986038\"/><status\r\n" + 
				"code=\"200.1083\" description=\"3d-acquirer:Cardholder Successfully authenticated.\" severity=\"information\" \r\n" + 
				"provider-transaction-id=\"C210079146944293986038\"/></statuses><csc-code>M</csc-code><requested-amount\r\n" + 
				"currency=\"JPY\">13800.0</requested-amount><account-holder><first-name></first-name><last-name>BriannaLKluckman</last-name><email>brikluckman@yahoo.com</email><phone>2402415793</phone><address><country>XX</country></address></account-holder><shipping><address><country>JP</country></address></shipping><card-token><token-id>4634397202557405</token-id><masked-account-number>427082******7405</masked-account-number></card-token><ip-address>120.29.149.56</ip-address><order-number>J1O116072573095/0</order-number><descriptor>H.I.S. \r\n" + 
				"Travel</descriptor><payment-methods><payment-method\r\n" + 
				"name=\"creditcard\"/></payment-methods><authorization-code>025238</authorization-code><three-d><cardholder-authentication-status>Y</cardholder-authentication-status></three-d><api-id>elastic-api</api-id><entry-mode>ecommerce</entry-mode></payment>";*/

		  JacksonXmlModule module = new JacksonXmlModule();
		    module.setDefaultUseWrapper(true);
		    XmlMapper xmlMapper = new XmlMapper(module);

		    // Configure it
		    xmlMapper
		        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		//XmlMapper xmlMapper = new XmlMapper();
		 Payment paymentRequest = xmlMapper.readValue(transctionRequest,Payment.class);
		 
		PaymentResponse paymentResponse=connectorService.processTransaction(paymentRequest);
		transactionResponse=xmlMapper.writeValueAsString(paymentResponse);
		System.out.println(" XML "+transactionResponse);
		return new ResponseEntity<String>(transactionResponse, HttpStatus.OK);
	}
}
