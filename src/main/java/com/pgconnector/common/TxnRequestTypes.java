package com.pgconnector.common;

/**
 * 
 * TransactionTypes enum for different transaction types
 * @author radhakrishnab
 *
 */

public enum TxnRequestTypes {

	SALE("AUTH", "Auth");


	private final String type;
	private final String description;

	private TxnRequestTypes(String type, String description) {
		this.type = type;
		this.description = description;
	}

	public static TxnRequestTypes getType(String type) {
		TxnRequestTypes[] transactionTypes = values();
		for (TxnRequestTypes transactionType : transactionTypes) {
			if (type.equals(transactionType.type)) {
				return transactionType;
			}
		}
		return null;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

}